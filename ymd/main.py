from jinja2 import Template
import yaml
import markdown
from io import StringIO
import os
from bs4 import BeautifulSoup


def parse_item(item, h):
    item_type = type(item)
    if item_type is list:
        if len(item) == 1:
            return parse_item(item[0], h + 1)
        if len(item) == 0:
            return ''
        return parse_item('\n'.join(['- ' + str(sub_item) for sub_item in item]), h + 1)
    if item_type is dict:
        return {
            "<h{0}>{1}</h{0}>".format(h, key): parse_item(value, h + 1) for key, value in item.items()
        }
    if item_type in [int, float]:
        return item
    return markdown.markdown(str(item))


def output(item, f):
    item_type = type(item)
    if item_type is list:
        [output(sub_item, f) for sub_item in item]
        return
    if item_type is dict:
        for key, value in item.items():
            output(key, f)
            output(value, f)
        return
    f.write(str(item))


with open('test.yml') as f:
    data = ''.join(f.read())
    template = Template(data, trim_blocks=True, lstrip_blocks=True)
    raw = template.render()
    formatted = list(yaml.load_all(raw))

with StringIO() as rendered:
    output(parse_item(formatted[1], 1), rendered)

    soup = BeautifulSoup(rendered.getvalue(), 'html.parser')
    os.remove('test.html')
    with open('test.html', 'w') as f:
        f.write(soup.prettify())
