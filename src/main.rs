use std::fs::File;
use std::io::Read;

fn read_file(path : &str) -> String {
    let mut data = File::open(path).unwrap();
    let mut buffer = String::new();
    data.read_to_string(&mut buffer);
    return buffer;
}

fn main() {
    let data = read_file("test.yml");
    println!("{:?}", data);
}
